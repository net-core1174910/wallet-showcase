﻿namespace Game_Wallet.Model
{    
    public enum TransactionType
    {
        deposit,
        stake,
        win
    }

    public enum TransactionState
    {
        inprogress,
        accepted,
        rejected
    }    
}
