﻿using Game_Wallet.Model;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Game_Wallet.Model
{
    [DataContract]
    public class TransactionContract
    {        
        [DataMember(Order = 0)]
        public Guid Guid { get; set; }

        [DataMember(Order =1)]
        public long Timestamp { get; private set; } = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
              
        [DataMember(Order = 2)]
        public decimal Amount { get; set; }

        [DataMember(Order = 3)]
        public TransactionType TransactionType { get; set; }

        [IgnoreDataMember]
        public TransactionState State { get; set; } = TransactionState.inprogress; 
    }

    
}
