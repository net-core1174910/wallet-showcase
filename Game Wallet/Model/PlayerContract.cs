﻿using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Game_Wallet.Model
{
    public class PlayerContract
    {
        public PlayerContract(string firstName, string lastName, string email)
        {            
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public PlayerContract(Guid guid, string firstName, string lastName, string email)
        {
            Guid = guid;
            FirstName = firstName;
            LastName = lastName;
            Email = email;           
        }
        [DataMember(Order = 0)]        
        public Guid Guid { get; private set; }

        [DataMember(Order = 1)]
        public string FirstName { get; set; }

        [DataMember(Order = 2)]
        public string LastName { get; set; }

        [DataMember(Order = 3)]
        [Required]
        public string Email { get; set; }
    }
}
