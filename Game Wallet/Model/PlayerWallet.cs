﻿using Game_Wallet.Utils;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Game_Wallet.Model
{
    [DataContract]
    public class PlayerWallet
    {
        public PlayerWallet(string firstName, string lastName, string email)
        {           
            FirstName = firstName;
            LastName = lastName;
            Email = email;           
        }

        public Guid Guid { get; private set; } = Guid.NewGuid();

        public string FirstName { get; private set; }
                
        public string LastName { get; private set; }
               
        public string Email { get; private set; }
                
        public decimal Balance { get; set; } = 0;

        public ConcurrentDictionary<Guid, TransactionContract> Transactions { get; set; } 
            = new ConcurrentDictionary<Guid, TransactionContract>();
                
    }
}
