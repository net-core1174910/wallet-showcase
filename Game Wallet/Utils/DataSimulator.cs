﻿using Game_Wallet.Model;
using Game_Wallet.Repository;

namespace Game_Wallet.Utils
{
    public class DataSimulator
    {
        private readonly TransactionProcessor transactionProcessor;

        public DataSimulator(IPlayerWalletRepository playerRepository, TransactionProcessor transactionProcessor)
        {            
            this.transactionProcessor = transactionProcessor;
        }

        public void PopulateWithPlayers(int numberOfPlayers, InMemoryPlayerRepository inMemoryPlayerRepository)
        {
            
            for(int i = 0; i < numberOfPlayers; i++)
            {               
                PlayerContract playerContract = new PlayerContract("John", "Doe", $"test{i}@test.com");
                PlayerWallet wallet = inMemoryPlayerRepository.CreatePlayer(playerContract);

                CreateRandomTransactionsForPlayer(wallet.Guid, inMemoryPlayerRepository);
            }            
        }

        public void CreateRandomTransactionsForPlayer(Guid playerGuid, InMemoryPlayerRepository inMemoryPlayerRepository)
        {
            Random random = new Random();
            decimal randomAmount;
            int transactionCount = random.Next(0, 10);
            var playerWallet = inMemoryPlayerRepository.GetPlayer(playerGuid);

            for (int i =0; i< transactionCount; i++)
            {
                randomAmount = (decimal)(random.NextDouble() * 1000);

                TransactionContract transactionContract = new TransactionContract() { Guid = Guid.NewGuid(), Amount = randomAmount, TransactionType = TransactionType.deposit, State = TransactionState.accepted };
                transactionProcessor.ProcessTransaction(playerWallet, transactionContract);                
            }
            
        }
    }
}
