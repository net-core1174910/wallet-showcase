﻿using Game_Wallet.Model;
using Game_Wallet.Repository;

namespace Game_Wallet.Utils
{
    public class TransactionProcessor
    {
        public TransactionState ProcessTransaction(PlayerWallet playerWallet, TransactionContract transactionContract)
        {
            //transaction not exists yet
            if (!playerWallet.Transactions.ContainsKey(transactionContract.Guid))
            {
                var resultState = CreateTransaction(playerWallet, transactionContract);
                return resultState;
            }
            //transaction duplicate
            else
            {
                return playerWallet.Transactions[transactionContract.Guid].State;
            }      
        }

        private TransactionState CreateTransaction(PlayerWallet playerWallet, TransactionContract transactionContract) 
        {
            TransactionState state = TransactionState.inprogress;
            try
            {
                //doesn't matter if client will use negative values for stake, we decide what happens with balance
                decimal amount = Math.Abs(transactionContract.Amount);

                if (transactionContract.TransactionType == TransactionType.stake)
                {
                    amount = -amount;
                }

                if (playerWallet.Balance + amount >= 0)
                {
                    playerWallet.Balance += amount;
                    state = TransactionState.accepted;
                }
                else
                {
                    state = TransactionState.rejected;
                }
            }
            catch
            {
                state = TransactionState.rejected;
            }
            finally
            {
                transactionContract.State = state;
                playerWallet.Transactions.TryAdd(transactionContract.Guid, transactionContract);
            }
            return state;
        }
    }
}