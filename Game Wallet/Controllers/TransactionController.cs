﻿using Game_Wallet.Model;
using Game_Wallet.Repository;
using Game_Wallet.Utils;
using Game_Wallet.Utils;
using Microsoft.AspNetCore.Mvc;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Game_Wallet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly IPlayerWalletRepository playerWalletRepository;
        private readonly TransactionProcessor transactionProcessor;

        public TransactionController(IPlayerWalletRepository playerRepository, TransactionProcessor transactionProcessor)
        {
            this.playerWalletRepository = playerRepository;
            this.transactionProcessor = transactionProcessor;
        }

       
        [HttpGet("GetAll")]              
        public IEnumerable<TransactionContract> Get(Guid guid)
        {
            return playerWalletRepository.GetAllTransactions(guid);
        }

        [HttpPost("GetByTypeForUser")]  
        public IEnumerable<TransactionContract> Get(Guid guid, [FromBody] TransactionType[] typesToFilter)
        {
            return playerWalletRepository.GetTransactionsByType(guid, typesToFilter);
        }


        [HttpPost("Transaction")]        
        public IActionResult Create(Guid playerGuid, [FromBody] TransactionContract tc)
        {
            var playerWallet = playerWalletRepository.GetPlayer(playerGuid);

            if (playerWallet == null)
            {
                return NotFound();
            }
            return Ok(transactionProcessor.ProcessTransaction(playerWallet, tc));
        } 
    }
}
