﻿using Game_Wallet.Model;
using Game_Wallet.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;


namespace Game_Wallet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerWalletController : ControllerBase
    {

        private readonly IPlayerWalletRepository playerRepository;

        public PlayerWalletController(IPlayerWalletRepository playerRepository)
        {
            this.playerRepository = playerRepository;
        }                    

        
        [HttpGet("GetByGuid")]        
        public PlayerWallet Get(Guid guid)
        {            
            return playerRepository.GetPlayer(guid);
        }

        
        [HttpGet("GetAll")]
        public IEnumerable<PlayerContract> GetAll()
        {
            return playerRepository.GetAllPlayers();
        }

        [HttpGet("GetBalance")]
        public decimal GetBalance(Guid guid)
        {
            return playerRepository.GetPlayer(guid).Balance;            
        }


        [HttpPost("CreateWallet")]
        public IActionResult CreatePlayerWallet([FromBody] PlayerContract pc)
        {
            if(pc == null)
            {
                throw new Exception("Invalid PlayerContract!");
            }
            var playerWallet = playerRepository.CreatePlayer(pc);
            if(playerWallet == null)
            {
                throw new Exception("Email adress already exists!");
            }
            return Created("", playerWallet);
        }
    }
}
