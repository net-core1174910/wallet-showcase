using Game_Wallet.Repository;
using Game_Wallet.Utils;
using System.Diagnostics;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Choose type of storage, currently we use temporary memory
builder.Services.AddSingleton<IPlayerWalletRepository, InMemoryPlayerRepository>();
builder.Services.AddSingleton<DataSimulator>();
builder.Services.AddSingleton<TransactionProcessor>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();


IPlayerWalletRepository repo = app.Services.GetRequiredService<IPlayerWalletRepository>(); 
if(repo is InMemoryPlayerRepository)
{    
    app.Services.GetRequiredService<DataSimulator>()
        .PopulateWithPlayers(200, (InMemoryPlayerRepository)repo);
}


app.Run();
