﻿using Game_Wallet.Model;
using Game_Wallet.Utils;
using System.Collections.Concurrent;
using System.Numerics;

namespace Game_Wallet.Repository
{
    public class InMemoryPlayerRepository : IPlayerWalletRepository
    {
        private ConcurrentDictionary<Guid, PlayerWallet> playersDict;

        public InMemoryPlayerRepository(ConcurrentDictionary<Guid, PlayerWallet> playersList)
        {
            this.playersDict = playersList;
        }
        public InMemoryPlayerRepository()
        {
            playersDict = new ConcurrentDictionary<Guid, PlayerWallet>();            
        }
        

        public PlayerWallet CreatePlayer(PlayerContract pc)
        {
            PlayerWallet playerWallet = new PlayerWallet(pc.FirstName, pc.LastName, pc.Email);

            bool userExists = playersDict.Values.
                Any(p => p.Email.Equals(playerWallet.Email,StringComparison.InvariantCultureIgnoreCase));
            
            if (userExists)
            {
                throw new Exception("Player with this email adress already exists!");
            }
            if (!playersDict.TryAdd(playerWallet.Guid, playerWallet))
            {
                throw new Exception("Cannot create player!");
            }
            return playerWallet;
        }

        public PlayerWallet GetPlayer(Guid playerGuid)
        {
            if (!playersDict.TryGetValue(playerGuid, out PlayerWallet player))
            {
                throw new Exception("Player wallet not exists!");
            }  
            
            return player;
        }

        public IEnumerable<PlayerContract> GetAllPlayers()
        {
            return playersDict.Values.Select(x=> new PlayerContract(x.Guid, x.FirstName, x.LastName, x.Email));
        }        

        public IEnumerable<TransactionContract> GetAllTransactions(Guid playerGuid)
        {
            return GetPlayer(playerGuid).Transactions.Values;
        }

        public IEnumerable<TransactionContract> GetTransactionsByType(Guid playerGuid, TransactionType[] transactionTypes)
        {
            return GetPlayer(playerGuid).Transactions.Values.Where(x=> transactionTypes.Contains(x.TransactionType));
        }

        public void AddTransaction(Guid playerGuid, TransactionContract transactionContract)
        {
            
            if (!GetPlayer(playerGuid).Transactions.TryAdd(transactionContract.Guid, transactionContract))
            {
                throw new Exception("Cannot finish transaction!");
            }
            
        }       
    }
}
