﻿using Game_Wallet.Model;
using System.Transactions;

namespace Game_Wallet.Repository
{
    public interface IPlayerWalletRepository
    {
        PlayerWallet CreatePlayer(PlayerContract playerContract);

        PlayerWallet GetPlayer(Guid playerGuid);

        IEnumerable<PlayerContract> GetAllPlayers();
              

        IEnumerable<TransactionContract> GetAllTransactions(Guid playerGuid);

        IEnumerable<TransactionContract> GetTransactionsByType(Guid playerGuid, TransactionType[] transactionTypes);

        void AddTransaction(Guid playerGuid, TransactionContract transaction);        
    }
}
