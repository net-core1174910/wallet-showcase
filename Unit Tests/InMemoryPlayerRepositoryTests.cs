using System.Collections.Concurrent;
using System.Transactions;
using Game_Wallet.Controllers;
using Game_Wallet.Model;
using Game_Wallet.Repository;

namespace Unit_Tests
{
    [TestFixture]
    public class InMemoryPlayerRepositoryTests
    {
        private ConcurrentDictionary<Guid, PlayerWallet> playersDict;
        private InMemoryPlayerRepository repository;

        [SetUp]
        public void Setup()
        {
            playersDict = new ConcurrentDictionary<Guid, PlayerWallet>();
            repository = new InMemoryPlayerRepository(playersDict);
        }

        [Test]
        public void CreatePlayer_AddsPlayerToDictionary()
        {
            // Arrange            
            var player = new PlayerContract("John", "Doe", "john.doe@example.com");

            // Act
            var playerWallet = repository.CreatePlayer(player);

            // Assert
            Assert.IsTrue(playersDict.ContainsKey(playerWallet.Guid));
            Assert.AreEqual(playerWallet, playersDict[playerWallet.Guid]);
        }

        [Test]
        public void CreatePlayer_ThrowsExceptionIfPlayerAlreadyExists()
        {
            // Arrange
            var player = new PlayerContract("John", "Doe", "john.doe@example.com");
            repository.CreatePlayer(player);

            // Act and Assert
            Assert.Throws<Exception>(() => repository.CreatePlayer(player));
        }

        [Test]
        public void GetPlayer_ReturnsPlayerIfItExists()
        {
            // Arrange
            var player = new PlayerWallet("John", "Doe", "john.doe@example.com");
            playersDict.TryAdd(player.Guid, player);

            // Act
            var result = repository.GetPlayer(player.Guid);

            // Assert
            Assert.AreEqual(player, result);
        }

        [Test]
        public void GetPlayer_ThrowsExceptionIfPlayerDoesNotExist()
        {
            // Arrange
            var playerGuid = Guid.NewGuid();

            // Act and Assert
            Assert.Throws<Exception>(() => repository.GetPlayer(playerGuid));
        }

        [Test]
        public void GetAllPlayers_ReturnsAllPlayers()
        {
            // Arrange
            var player1 = new PlayerWallet("John", "Doe", "john.doe@example.com");
            var player2 = new PlayerWallet("Jane", "Doe", "jane.doe@example.com");
            playersDict.TryAdd(player1.Guid, player1);
            playersDict.TryAdd(player2.Guid, player2);

            // Act
            var result = repository.GetAllPlayers();

            // Assert
            Assert.AreEqual(2, result.Count());
            Assert.IsTrue(result.Any(p => p.Guid == player1.Guid));
            Assert.IsTrue(result.Any(p => p.Guid == player2.Guid));
        }

        [Test]
        public void GetAllTransactions_ReturnsAllTransactionsForPlayer()
        {
            // Arrange            
            var player = new PlayerWallet("John", "Doe", "john.doe@example.com");

            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 100, TransactionType = TransactionType.deposit, State = TransactionState.accepted });
            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 50, TransactionType = TransactionType.stake, State = TransactionState.accepted });

            playersDict.TryAdd(player.Guid, player);

            // Act
            var result = repository.GetAllTransactions(player.Guid);

            // Assert
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public void GetTransactionsByType_ReturnsTransactionsWithMatchingTypes()
        {
            // Arrange            
            var player = new PlayerWallet("John", "Doe", "john.doe@example.com");
            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 200, TransactionType = TransactionType.deposit, State = TransactionState.accepted });
            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 50, TransactionType = TransactionType.stake, State = TransactionState.accepted });
            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 100, TransactionType = TransactionType.stake, State = TransactionState.accepted });
            player.Transactions.TryAdd(Guid.NewGuid(), new TransactionContract() { Guid = Guid.NewGuid(), Amount = 50, TransactionType = TransactionType.win, State = TransactionState.accepted });

            playersDict.TryAdd(player.Guid, player);
            var result = repository.GetTransactionsByType(player.Guid, new TransactionType[] { TransactionType.stake });

            // Assert
            Assert.AreEqual(2, result.Count());
        }
    }
}